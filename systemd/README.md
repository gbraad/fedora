Fedora systemd
==============


This container is based on information published [here](https://vpavlin.eu/2015/02/fedora-docker-and-systemd/).

## Usage

```
$ docker build -t fedora:systemd .
$ docker run \
  --name systemd \
  -d \
  -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
  fedora:systemd
```
